﻿INSTRUCTIONS
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Name: You put the name of the prop the .ytyp is creating it for. //DO NOT DO THIS IF YOU ARE USING AUTO CALCULATING BB'S AS IT WILL FILL IT OUT FOR YOU.

Object Rpf: You put the name of the .rpf that the prop will be stored in.
So for example the prop is in example.rpf you put example in the textbox

Flags = Set your flags to what is needed there are 2 default flags in the
drop down, 32 and 8192 which are most common.

Special Attributes: If there are none just place 0.

Hd texture dist: Should be half of LOD dist //DO NOT DO THIS IF YOU ARE USING AUTO CALCULATING BB'S AS IT WILL FILL IT OUT FOR YOU.

LOD dist: Input you lod distance. //DO NOT DO THIS IF YOU ARE USING AUTO CALCULATING BB'S AS IT WILL FILL IT OUT FOR YOU.

For all BB's Fill in the info needed if you want to calculate BB's using a .odr
then refer to autocalculating BB's instuction below.

Only check Using Texture Dictionary if you are using an external .ytd for the prop.
Otherwise dont check it.

Only check Using Drawable Dictionary if you are using a drawable dictionary.

Only check Physics Dictionary if you have embedded collisions in the prop. 

Once you fill out all the textboxes in the top right corner you will see a button called Add Item this will add all the info you typed into an <item> node in the .ytyp document
you will then need to click create button this will create the actually ytyp. All textboxes and checkboxes will be reset you can now proceed to adding a new item. Just fill out
all the textboxes then click the Add Item Button and this will add all the info to the .ytyp document once again. Click the Create button once again and this will add another 
<item> node for that object you just filled out. Continue this proccess till all objects are finished.

Once you create the .ytyp you will see a View Created Ytyp button. CLick that if you want to view it or edit it. In there you can edit/save anything just in case.

The YTYP created will be located in the directory of my program. By default it is named "template.ytyp" you can change this when you import into OpenIV. With the new update if you 
you can click Custom YTYP name and enter the name that you want the YTYP to be called.

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
HOW TO AUTO CALCULATE BB'S USING THE ODR BROWSER

First open the Odr Browser by clicking open > odr broswer. Now Click on Open 
and browse to where you .odr is for the prop you are creating the .ytyp for 
Once you click on it it will import everything next just click BB'S and Calculate BB's.
This is all you need to do in here you can view the BB's by click BB's > View > BB (whichever one you want to view).
Next just click close and once in the ytyp creator again just click file > Import BB's 
You will see all BB's info and the name of the prop is filled out. Now just fill out the rest using 
the instructions above and your done. 
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
