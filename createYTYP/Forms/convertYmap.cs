﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Xml;

namespace createYTYP
{
    public partial class convertYmap : Form
    {

        OpenFileDialog openFile = new OpenFileDialog();
        SaveFileDialog saveFile = new SaveFileDialog();
        string line = "";
#pragma warning disable CS0414 // The field 'convertYmap.strData' is assigned but its value is never used
        string strData = "";
#pragma warning restore CS0414 // The field 'convertYmap.strData' is assigned but its value is never used
        public convertYmap()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenYMAP();
        }

        private void OpenYMAP()
        {
            if (openFile.ShowDialog() == DialogResult.OK)
            {
                StreamReader sr = new StreamReader(openFile.FileName);
                while (line != null)
                {
                    line = sr.ReadLine();
                    if (line != null)
                    {
                        listBox1.Items.Add(line);
                    }
                }
                sr.Close();
            }
        }

        private void convertYmap_Load(object sender, EventArgs e)
        {
            openFile.Filter = "Ymap Files (.ymap)| *.ymap";
        }

        private void button2_Click(object sender, EventArgs e)
        {

            FileStream st = new FileStream(openFile.FileName, FileMode.Open);
            byte[] buffer = new byte[st.Length];
            st.Read(buffer, 0, (int)st.Length);
            st.Close();

            XmlDocument myXML = new XmlDocument();
            MemoryStream ms = new MemoryStream(buffer);
            myXML.Load(ms);
            myXML.Save(Application.StartupPath + "\\myfile.xml");
        }
    }
}
