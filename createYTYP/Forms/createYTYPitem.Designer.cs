﻿namespace createYTYP
{
    partial class Create
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Create));
            this.createButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.bbMinZ_TB = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.bbMinY_TB = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.bbMinX_TB = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.bbMaxZ_TB = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.bbMaxY_TB = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.bbMaxX_TB = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.bsCentreZ_TB = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.bsCentreY_TB = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.bsCentreX_TB = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.bsRadius_TB = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.name_TB = new System.Windows.Forms.TextBox();
            this.isUsingTextureDictionary_CB = new System.Windows.Forms.CheckBox();
            this.textureDictionary_TB = new System.Windows.Forms.TextBox();
            this.isUsingDrawableDictionary_CB = new System.Windows.Forms.CheckBox();
            this.drawableDictionary_TB = new System.Windows.Forms.TextBox();
            this.isUsingPhysicsDictionary_CB = new System.Windows.Forms.CheckBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.odrBrowserToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.label20 = new System.Windows.Forms.Label();
            this.assetType_TB = new System.Windows.Forms.ComboBox();
            this.label21 = new System.Windows.Forms.Label();
            this.location_TB = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.lodDist_TB = new System.Windows.Forms.ComboBox();
            this.flags_TB = new System.Windows.Forms.ComboBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.ViewCreatedYtypButton = new System.Windows.Forms.Button();
            this.hdTextureDist_TB = new System.Windows.Forms.ComboBox();
            this.specialAttributes_TB = new System.Windows.Forms.ComboBox();
            this.embeddedCols_TB = new System.Windows.Forms.TextBox();
            this.openToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.importBBsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.optionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clearEverythingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newYTYPToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // createButton
            // 
            this.createButton.Location = new System.Drawing.Point(435, 546);
            this.createButton.Name = "createButton";
            this.createButton.Size = new System.Drawing.Size(92, 40);
            this.createButton.TabIndex = 0;
            this.createButton.Text = "Create";
            this.toolTip1.SetToolTip(this.createButton, "Click to generate your .ytyp");
            this.createButton.UseVisualStyleBackColor = true;
            this.createButton.Click += new System.EventHandler(this.createButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(45, 257);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 15);
            this.label1.TabIndex = 2;
            this.label1.Text = "LOD Dist:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(68, 140);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 15);
            this.label2.TabIndex = 4;
            this.label2.Text = "Flags:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(5, 166);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(104, 15);
            this.label3.TabIndex = 6;
            this.label3.Text = "Special Attributes:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(87, 346);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(17, 15);
            this.label4.TabIndex = 12;
            this.label4.Text = "Z:";
            // 
            // bbMinZ_TB
            // 
            this.bbMinZ_TB.Location = new System.Drawing.Point(110, 346);
            this.bbMinZ_TB.Name = "bbMinZ_TB";
            this.bbMinZ_TB.Size = new System.Drawing.Size(195, 20);
            this.bbMinZ_TB.TabIndex = 11;
            this.toolTip1.SetToolTip(this.bbMinZ_TB, "Enter bbMIN Z");
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(87, 321);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(17, 15);
            this.label5.TabIndex = 10;
            this.label5.Text = "Y:";
            // 
            // bbMinY_TB
            // 
            this.bbMinY_TB.Location = new System.Drawing.Point(110, 320);
            this.bbMinY_TB.Name = "bbMinY_TB";
            this.bbMinY_TB.Size = new System.Drawing.Size(195, 20);
            this.bbMinY_TB.TabIndex = 9;
            this.toolTip1.SetToolTip(this.bbMinY_TB, "Enter bbMIN Y");
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(87, 297);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(18, 15);
            this.label6.TabIndex = 8;
            this.label6.Text = "X:";
            // 
            // bbMinX_TB
            // 
            this.bbMinX_TB.Location = new System.Drawing.Point(110, 294);
            this.bbMinX_TB.Name = "bbMinX_TB";
            this.bbMinX_TB.Size = new System.Drawing.Size(195, 20);
            this.bbMinX_TB.TabIndex = 7;
            this.toolTip1.SetToolTip(this.bbMinX_TB, "Enter bbMIN X");
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(36, 294);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(45, 15);
            this.label7.TabIndex = 13;
            this.label7.Text = "bbMin:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(33, 392);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(48, 15);
            this.label8.TabIndex = 20;
            this.label8.Text = "bbMax:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(87, 439);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(17, 15);
            this.label9.TabIndex = 19;
            this.label9.Text = "Z:";
            // 
            // bbMaxZ_TB
            // 
            this.bbMaxZ_TB.Location = new System.Drawing.Point(110, 439);
            this.bbMaxZ_TB.Name = "bbMaxZ_TB";
            this.bbMaxZ_TB.Size = new System.Drawing.Size(195, 20);
            this.bbMaxZ_TB.TabIndex = 18;
            this.toolTip1.SetToolTip(this.bbMaxZ_TB, "Enter bbMAX Z");
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(87, 414);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(17, 15);
            this.label10.TabIndex = 17;
            this.label10.Text = "Y:";
            // 
            // bbMaxY_TB
            // 
            this.bbMaxY_TB.Location = new System.Drawing.Point(110, 413);
            this.bbMaxY_TB.Name = "bbMaxY_TB";
            this.bbMaxY_TB.Size = new System.Drawing.Size(195, 20);
            this.bbMaxY_TB.TabIndex = 16;
            this.toolTip1.SetToolTip(this.bbMaxY_TB, "Enter bbMAX Y");
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(87, 390);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(18, 15);
            this.label11.TabIndex = 15;
            this.label11.Text = "X:";
            // 
            // bbMaxX_TB
            // 
            this.bbMaxX_TB.Location = new System.Drawing.Point(110, 387);
            this.bbMaxX_TB.Name = "bbMaxX_TB";
            this.bbMaxX_TB.Size = new System.Drawing.Size(195, 20);
            this.bbMaxX_TB.TabIndex = 14;
            this.toolTip1.SetToolTip(this.bbMaxX_TB, "Enter bbMAX X");
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(21, 477);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(60, 15);
            this.label12.TabIndex = 27;
            this.label12.Text = "bbCentre:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(87, 526);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(17, 15);
            this.label13.TabIndex = 26;
            this.label13.Text = "Z:";
            // 
            // bsCentreZ_TB
            // 
            this.bsCentreZ_TB.Location = new System.Drawing.Point(110, 526);
            this.bsCentreZ_TB.Name = "bsCentreZ_TB";
            this.bsCentreZ_TB.Size = new System.Drawing.Size(195, 20);
            this.bsCentreZ_TB.TabIndex = 25;
            this.toolTip1.SetToolTip(this.bsCentreZ_TB, "Enter bbCentre Z");
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(87, 501);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(17, 15);
            this.label14.TabIndex = 24;
            this.label14.Text = "Y:";
            // 
            // bsCentreY_TB
            // 
            this.bsCentreY_TB.Location = new System.Drawing.Point(110, 500);
            this.bsCentreY_TB.Name = "bsCentreY_TB";
            this.bsCentreY_TB.Size = new System.Drawing.Size(195, 20);
            this.bsCentreY_TB.TabIndex = 23;
            this.toolTip1.SetToolTip(this.bsCentreY_TB, "Enter bbCentre Y");
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(87, 477);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(18, 15);
            this.label15.TabIndex = 22;
            this.label15.Text = "X:";
            // 
            // bsCentreX_TB
            // 
            this.bsCentreX_TB.Location = new System.Drawing.Point(110, 474);
            this.bsCentreX_TB.Name = "bsCentreX_TB";
            this.bsCentreX_TB.Size = new System.Drawing.Size(195, 20);
            this.bsCentreX_TB.TabIndex = 21;
            this.toolTip1.SetToolTip(this.bsCentreX_TB, "Enter bbCentre X");
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(38, 205);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(68, 15);
            this.label16.TabIndex = 29;
            this.label16.Text = "BS Radius:";
            // 
            // bsRadius_TB
            // 
            this.bsRadius_TB.Location = new System.Drawing.Point(110, 202);
            this.bsRadius_TB.Name = "bsRadius_TB";
            this.bsRadius_TB.Size = new System.Drawing.Size(195, 20);
            this.bsRadius_TB.TabIndex = 28;
            this.toolTip1.SetToolTip(this.bsRadius_TB, "Enter bsradius");
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(8, 231);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(96, 15);
            this.label17.TabIndex = 31;
            this.label17.Text = "HD Texture Dist:";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(62, 89);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(44, 15);
            this.label19.TabIndex = 35;
            this.label19.Text = "Name:";
            // 
            // name_TB
            // 
            this.name_TB.Location = new System.Drawing.Point(110, 86);
            this.name_TB.Name = "name_TB";
            this.name_TB.Size = new System.Drawing.Size(195, 20);
            this.name_TB.TabIndex = 34;
            this.toolTip1.SetToolTip(this.name_TB, "Enter model name");
            // 
            // isUsingTextureDictionary_CB
            // 
            this.isUsingTextureDictionary_CB.AutoSize = true;
            this.isUsingTextureDictionary_CB.Location = new System.Drawing.Point(311, 136);
            this.isUsingTextureDictionary_CB.Name = "isUsingTextureDictionary_CB";
            this.isUsingTextureDictionary_CB.Size = new System.Drawing.Size(207, 19);
            this.isUsingTextureDictionary_CB.TabIndex = 36;
            this.isUsingTextureDictionary_CB.Text = "Using External Texture Dictionary";
            this.toolTip1.SetToolTip(this.isUsingTextureDictionary_CB, "Check to enable external texture dictionary");
            this.isUsingTextureDictionary_CB.UseVisualStyleBackColor = true;
            this.isUsingTextureDictionary_CB.CheckedChanged += new System.EventHandler(this.isUsingTextureDictionary_CheckedChanged);
            // 
            // textureDictionary_TB
            // 
            this.textureDictionary_TB.Location = new System.Drawing.Point(329, 161);
            this.textureDictionary_TB.Name = "textureDictionary_TB";
            this.textureDictionary_TB.Size = new System.Drawing.Size(195, 20);
            this.textureDictionary_TB.TabIndex = 37;
            this.toolTip1.SetToolTip(this.textureDictionary_TB, "Enter Texture Dictionary");
            this.textureDictionary_TB.Visible = false;
            // 
            // isUsingDrawableDictionary_CB
            // 
            this.isUsingDrawableDictionary_CB.AutoSize = true;
            this.isUsingDrawableDictionary_CB.Location = new System.Drawing.Point(311, 187);
            this.isUsingDrawableDictionary_CB.Name = "isUsingDrawableDictionary_CB";
            this.isUsingDrawableDictionary_CB.Size = new System.Drawing.Size(216, 19);
            this.isUsingDrawableDictionary_CB.TabIndex = 38;
            this.isUsingDrawableDictionary_CB.Text = "Using Custom Drawable Dictionary";
            this.toolTip1.SetToolTip(this.isUsingDrawableDictionary_CB, "Check to enable drawable dictionary");
            this.isUsingDrawableDictionary_CB.UseVisualStyleBackColor = true;
            this.isUsingDrawableDictionary_CB.CheckedChanged += new System.EventHandler(this.isUsingDrawableDictionary_CheckedChanged);
            // 
            // drawableDictionary_TB
            // 
            this.drawableDictionary_TB.Location = new System.Drawing.Point(329, 212);
            this.drawableDictionary_TB.Name = "drawableDictionary_TB";
            this.drawableDictionary_TB.Size = new System.Drawing.Size(195, 20);
            this.drawableDictionary_TB.TabIndex = 39;
            this.toolTip1.SetToolTip(this.drawableDictionary_TB, "Enter Drawable Dictionary");
            this.drawableDictionary_TB.Visible = false;
            // 
            // isUsingPhysicsDictionary_CB
            // 
            this.isUsingPhysicsDictionary_CB.AutoSize = true;
            this.isUsingPhysicsDictionary_CB.Location = new System.Drawing.Point(311, 87);
            this.isUsingPhysicsDictionary_CB.Name = "isUsingPhysicsDictionary_CB";
            this.isUsingPhysicsDictionary_CB.Size = new System.Drawing.Size(140, 19);
            this.isUsingPhysicsDictionary_CB.TabIndex = 40;
            this.isUsingPhysicsDictionary_CB.Text = "Embedded Collsions";
            this.toolTip1.SetToolTip(this.isUsingPhysicsDictionary_CB, "Check this if your model has embedded collisions");
            this.isUsingPhysicsDictionary_CB.UseVisualStyleBackColor = true;
            this.isUsingPhysicsDictionary_CB.CheckedChanged += new System.EventHandler(this.isUsingPhysicsDictionary_CheckedChanged);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.odrBrowserToolStripMenuItem1,
            this.optionsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(540, 25);
            this.menuStrip1.TabIndex = 43;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // odrBrowserToolStripMenuItem1
            // 
            this.odrBrowserToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem1,
            this.importBBsToolStripMenuItem1});
            this.odrBrowserToolStripMenuItem1.Name = "odrBrowserToolStripMenuItem1";
            this.odrBrowserToolStripMenuItem1.Size = new System.Drawing.Size(94, 21);
            this.odrBrowserToolStripMenuItem1.Text = "Odr Browser";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(40, 565);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(68, 15);
            this.label20.TabIndex = 45;
            this.label20.Text = "Asset Type:";
            // 
            // assetType_TB
            // 
            this.assetType_TB.FormattingEnabled = true;
            this.assetType_TB.Items.AddRange(new object[] {
            "ASSET_TYPE_DRAWABLE"});
            this.assetType_TB.Location = new System.Drawing.Point(110, 562);
            this.assetType_TB.Name = "assetType_TB";
            this.assetType_TB.Size = new System.Drawing.Size(195, 21);
            this.assetType_TB.TabIndex = 46;
            this.assetType_TB.Text = "ASSET_TYPE_DRAWABLE";
            this.toolTip1.SetToolTip(this.assetType_TB, "Select the Asset Type");
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(30, 115);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(72, 15);
            this.label21.TabIndex = 48;
            this.label21.Text = "Object RPF:";
            // 
            // location_TB
            // 
            this.location_TB.Location = new System.Drawing.Point(110, 112);
            this.location_TB.Name = "location_TB";
            this.location_TB.Size = new System.Drawing.Size(195, 20);
            this.location_TB.TabIndex = 47;
            this.toolTip1.SetToolTip(this.location_TB, "Enter the location this .ytyp will reside");
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 23.75258F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(121, 25);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(297, 38);
            this.label18.TabIndex = 42;
            this.label18.Text = "Create YTYP Item";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(437, 38);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 49;
            this.button2.Text = "Add Item";
            this.toolTip1.SetToolTip(this.button2, "NOT AVAILABLE");
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.newItem_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.BackColor = System.Drawing.SystemColors.Control;
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 596);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(540, 22);
            this.statusStrip1.TabIndex = 50;
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(217, 17);
            this.toolStripStatusLabel1.Text = "YTYP Generator - Author @Skylumz ";
            // 
            // lodDist_TB
            // 
            this.lodDist_TB.FormattingEnabled = true;
            this.lodDist_TB.Items.AddRange(new object[] {
            "200",
            "2000",
            "20000",
            "200000",
            "2000000"});
            this.lodDist_TB.Location = new System.Drawing.Point(110, 257);
            this.lodDist_TB.Name = "lodDist_TB";
            this.lodDist_TB.Size = new System.Drawing.Size(195, 21);
            this.lodDist_TB.TabIndex = 51;
            this.toolTip1.SetToolTip(this.lodDist_TB, "Select the Asset Type");
            // 
            // flags_TB
            // 
            this.flags_TB.FormattingEnabled = true;
            this.flags_TB.Items.AddRange(new object[] {
            "32",
            "8192"});
            this.flags_TB.Location = new System.Drawing.Point(110, 136);
            this.flags_TB.Name = "flags_TB";
            this.flags_TB.Size = new System.Drawing.Size(195, 21);
            this.flags_TB.TabIndex = 52;
            this.toolTip1.SetToolTip(this.flags_TB, "Set the flags");
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(391, 592);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(137, 19);
            this.checkBox1.TabIndex = 55;
            this.checkBox1.Text = "Custom YTYP Name";
            this.toolTip1.SetToolTip(this.checkBox1, "Check to enable physics dictionary");
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.isUsingCustomFile_CheckedChanged);
            // 
            // ViewCreatedYtypButton
            // 
            this.ViewCreatedYtypButton.Location = new System.Drawing.Point(337, 546);
            this.ViewCreatedYtypButton.Name = "ViewCreatedYtypButton";
            this.ViewCreatedYtypButton.Size = new System.Drawing.Size(92, 40);
            this.ViewCreatedYtypButton.TabIndex = 56;
            this.ViewCreatedYtypButton.Text = "View Created Ytyp";
            this.toolTip1.SetToolTip(this.ViewCreatedYtypButton, "Click to generate your .ytyp");
            this.ViewCreatedYtypButton.UseVisualStyleBackColor = true;
            this.ViewCreatedYtypButton.Visible = false;
            this.ViewCreatedYtypButton.Click += new System.EventHandler(this.ViewCreatedYtypButton_Click);
            // 
            // hdTextureDist_TB
            // 
            this.hdTextureDist_TB.FormattingEnabled = true;
            this.hdTextureDist_TB.Items.AddRange(new object[] {
            "100",
            "1000",
            "10000",
            "100000",
            "1000000"});
            this.hdTextureDist_TB.Location = new System.Drawing.Point(110, 228);
            this.hdTextureDist_TB.Name = "hdTextureDist_TB";
            this.hdTextureDist_TB.Size = new System.Drawing.Size(195, 21);
            this.hdTextureDist_TB.TabIndex = 57;
            this.toolTip1.SetToolTip(this.hdTextureDist_TB, "Select the Asset Type");
            // 
            // specialAttributes_TB
            // 
            this.specialAttributes_TB.FormattingEnabled = true;
            this.specialAttributes_TB.Items.AddRange(new object[] {
            "0"});
            this.specialAttributes_TB.Location = new System.Drawing.Point(110, 163);
            this.specialAttributes_TB.Name = "specialAttributes_TB";
            this.specialAttributes_TB.Size = new System.Drawing.Size(195, 21);
            this.specialAttributes_TB.TabIndex = 58;
            this.specialAttributes_TB.Text = "0";
            this.toolTip1.SetToolTip(this.specialAttributes_TB, "Select the Asset Type");
            // 
            // embeddedCols_TB
            // 
            this.embeddedCols_TB.Location = new System.Drawing.Point(329, 110);
            this.embeddedCols_TB.Name = "embeddedCols_TB";
            this.embeddedCols_TB.Size = new System.Drawing.Size(195, 20);
            this.embeddedCols_TB.TabIndex = 59;
            this.toolTip1.SetToolTip(this.embeddedCols_TB, "Enter Texture Dictionary");
            // 
            // openToolStripMenuItem1
            // 
            this.openToolStripMenuItem1.Name = "openToolStripMenuItem1";
            this.openToolStripMenuItem1.Size = new System.Drawing.Size(153, 22);
            this.openToolStripMenuItem1.Text = "Open";
            this.openToolStripMenuItem1.Click += new System.EventHandler(this.openToolStripMenuItem1_Click);
            // 
            // importBBsToolStripMenuItem1
            // 
            this.importBBsToolStripMenuItem1.Name = "importBBsToolStripMenuItem1";
            this.importBBsToolStripMenuItem1.Size = new System.Drawing.Size(153, 22);
            this.importBBsToolStripMenuItem1.Text = "Import BB\'s";
            this.importBBsToolStripMenuItem1.Click += new System.EventHandler(this.importBBsToolStripMenuItem1_Click);
            // 
            // optionsToolStripMenuItem
            // 
            this.optionsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.clearEverythingToolStripMenuItem,
            this.newYTYPToolStripMenuItem});
            this.optionsToolStripMenuItem.Name = "optionsToolStripMenuItem";
            this.optionsToolStripMenuItem.Size = new System.Drawing.Size(66, 21);
            this.optionsToolStripMenuItem.Text = "Options";
            // 
            // clearEverythingToolStripMenuItem
            // 
            this.clearEverythingToolStripMenuItem.Name = "clearEverythingToolStripMenuItem";
            this.clearEverythingToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.clearEverythingToolStripMenuItem.Text = "Clear Textboxes";
            this.clearEverythingToolStripMenuItem.Click += new System.EventHandler(this.clearEverythingToolStripMenuItem_Click);
            // 
            // newYTYPToolStripMenuItem
            // 
            this.newYTYPToolStripMenuItem.Name = "newYTYPToolStripMenuItem";
            this.newYTYPToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.newYTYPToolStripMenuItem.Text = "New YTYP";
            this.newYTYPToolStripMenuItem.Click += new System.EventHandler(this.newYTYPToolStripMenuItem_Click);
            // 
            // Create
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(540, 618);
            this.Controls.Add(this.embeddedCols_TB);
            this.Controls.Add(this.specialAttributes_TB);
            this.Controls.Add(this.hdTextureDist_TB);
            this.Controls.Add(this.ViewCreatedYtypButton);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.flags_TB);
            this.Controls.Add(this.lodDist_TB);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.location_TB);
            this.Controls.Add(this.assetType_TB);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.isUsingPhysicsDictionary_CB);
            this.Controls.Add(this.drawableDictionary_TB);
            this.Controls.Add(this.isUsingDrawableDictionary_CB);
            this.Controls.Add(this.textureDictionary_TB);
            this.Controls.Add(this.isUsingTextureDictionary_CB);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.name_TB);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.bsRadius_TB);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.bsCentreZ_TB);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.bsCentreY_TB);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.bsCentreX_TB);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.bbMaxZ_TB);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.bbMaxY_TB);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.bbMaxX_TB);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.bbMinZ_TB);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.bbMinY_TB);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.bbMinX_TB);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.createButton);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(556, 657);
            this.MinimumSize = new System.Drawing.Size(556, 657);
            this.Name = "Create";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CreateYTYP";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox bbMaxZ_TB;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox bsCentreX_TB;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.CheckBox isUsingDrawableDictionary_CB;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolTip toolTip1;
        public System.Windows.Forms.Button createButton;
        public System.Windows.Forms.TextBox bbMinZ_TB;
        public System.Windows.Forms.TextBox bbMinY_TB;
        public System.Windows.Forms.TextBox bbMinX_TB;
        public System.Windows.Forms.TextBox bbMaxY_TB;
        public System.Windows.Forms.TextBox bbMaxX_TB;
        public System.Windows.Forms.TextBox bsCentreZ_TB;
        public System.Windows.Forms.TextBox bsCentreY_TB;
        public System.Windows.Forms.TextBox bsRadius_TB;
        public System.Windows.Forms.TextBox name_TB;
        public System.Windows.Forms.CheckBox isUsingTextureDictionary_CB;
        public System.Windows.Forms.TextBox textureDictionary_TB;
        public System.Windows.Forms.TextBox drawableDictionary_TB;
        public System.Windows.Forms.CheckBox isUsingPhysicsDictionary_CB;
        public System.Windows.Forms.ComboBox assetType_TB;
        public System.Windows.Forms.TextBox location_TB;
        public System.Windows.Forms.Button button2;
        public System.Windows.Forms.ComboBox lodDist_TB;
        public System.Windows.Forms.ComboBox flags_TB;
        public System.Windows.Forms.CheckBox checkBox1;
        public System.Windows.Forms.Button ViewCreatedYtypButton;
        public System.Windows.Forms.ComboBox hdTextureDist_TB;
        public System.Windows.Forms.ComboBox specialAttributes_TB;
        public System.Windows.Forms.TextBox embeddedCols_TB;
        private System.Windows.Forms.ToolStripMenuItem odrBrowserToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem importBBsToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem optionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clearEverythingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newYTYPToolStripMenuItem;
    }
}

