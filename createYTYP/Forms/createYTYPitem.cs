﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.IO;

namespace createYTYP
{
    public partial class Create : Form
    {
        public Create()
        {
            InitializeComponent();
        }

        public static XmlDocument tempDoc = new XmlDocument();

        //for importing BB's
        public static string I_bbminName;
        public static string I_bbminX;
        public static string I_bbminY;
        public static string I_bbminZ;

        public static string I_bbmaxName;
        public static string I_bbmaxX;
        public static string I_bbmaxY;
        public static string I_bbmaxZ;

        public static string I_centerName;
        public static string I_centerX;
        public static string I_centerY;
        public static string I_centerZ;

        public static string I_radiusName;
        public static string I_radius;

        public static string I_modelName;

        public static string lodDist = "0";
        public static string flags = "0";
        public static string specialAttributes = "0";
        public static string hdTextureDist = "0";
        public static string name = "null";
        public static string textureDictionaryName = "0";
        public static string textureDictionary = "0";
        public static string physicsDictionaryName = "0";
        public static string physicsDictionary = "0";
        public static string assetType = "0";

        public static bool isUsingTextureDict = false;
        public static bool isUsingDrawableDict = false;
        public static bool isUsingPhysicsDict = false;
        public static bool isUsingFileName = false;

        public static string fileName = "template";
        public static string filePathSavedTo = "";
        public static bool createdYtyp = false;
        public static bool isfn = false;

        //SavesXML
        private void SaveXML()
        {
            try
            {
                tempDoc.Save(Application.StartupPath + "\\" + fileName + ".ytyp.xml");

                filePathSavedTo = Application.StartupPath + "\\" + fileName + ".ytyp.xml";
                createdYtyp = true;
                ShowViewCreatedYtypButton();

                MessageBox.Show("Successfully created " + fileName + ".ytyp!", "Success");
            }
            catch
            {
                MessageBox.Show("You have to add a prop before you can create a ytyp!", "Error");
            }
        }

        //Creates item node with all data from form
        private void CreateItemNode()
        {
            //xmlnode paths
            XmlNode archetypesNode = tempDoc.SelectSingleNode("CMapTypes/archetypes");
            XmlNode itemNode = tempDoc.CreateElement("Item");
            XmlNode itemNodeLodDist = tempDoc.CreateElement("lodDist");
            XmlNode itemNodeFlags = tempDoc.CreateElement("flags");
            XmlNode itemNodeSpecialAttributes = tempDoc.CreateElement("specialAttribute");
            XmlNode itemNodebbMin = tempDoc.CreateElement("bbMin");
            XmlNode itemNodebbMax = tempDoc.CreateElement("bbMax");
            XmlNode itemNodebsCentre = tempDoc.CreateElement("bsCentre");
            XmlNode itemNodebsRadius = tempDoc.CreateElement("bsRadius");
            XmlNode itemNodehdTextureDist = tempDoc.CreateElement("hdTextureDist");
            XmlNode itemNodeName = tempDoc.CreateElement("name");
            XmlNode itemNodetextureDictionary = tempDoc.CreateElement("textureDictionary");
            XmlNode itemNodeclipDictionary = tempDoc.CreateElement("clipDictionary");
            XmlNode itemNodeDrawableDictionary = tempDoc.CreateElement("drawableDictionary");
            XmlNode itemNodePhysicsDictionary = tempDoc.CreateElement("physicsDictionary");
            XmlNode itemNodeAssetType = tempDoc.CreateElement("assetType");
            XmlNode itemNodeAssetName = tempDoc.CreateElement("assetName");
            XmlNode itemNodeExtensions = tempDoc.CreateElement("extensions");
            XmlNode CmapNodeName = tempDoc.SelectSingleNode("CMapTypes/name");

            //item
            var val10 = tempDoc.CreateAttribute("type");
            val10.Value = "CBaseArchetypeDef";
            itemNode.Attributes.Append(val10);
            archetypesNode.AppendChild(itemNode);

            //lodDist
            var val1 = tempDoc.CreateAttribute("value");
            val1.Value = lodDist;
            itemNodeLodDist.Attributes.Append(val1);
            itemNode.AppendChild(itemNodeLodDist);

            //flags
            var val2 = tempDoc.CreateAttribute("value");
            val2.Value = flags;
            itemNodeFlags.Attributes.Append(val2);
            itemNode.AppendChild(itemNodeFlags);

            //specialAttributes
            var val3 = tempDoc.CreateAttribute("value");
            val3.Value = specialAttributes;
            itemNodeSpecialAttributes.Attributes.Append(val3);
            itemNode.AppendChild(itemNodeSpecialAttributes);

            //bbMin
            var bbX1 = tempDoc.CreateAttribute("x");
            bbX1.Value = I_bbminX;
            itemNodebbMin.Attributes.Append(bbX1);
            var bbY1 = tempDoc.CreateAttribute("y");
            bbY1.Value = I_bbminY;
            itemNodebbMin.Attributes.Append(bbY1);
            var bbZ1 = tempDoc.CreateAttribute("z");
            bbZ1.Value = I_bbminZ;
            itemNodebbMin.Attributes.Append(bbZ1);
            itemNode.AppendChild(itemNodebbMin);

            //bbMax
            var bbX2 = tempDoc.CreateAttribute("x");
            bbX2.Value = I_bbmaxX;
            itemNodebbMax.Attributes.Append(bbX2);
            var bbY2 = tempDoc.CreateAttribute("y");
            bbY2.Value = I_bbmaxY;
            itemNodebbMax.Attributes.Append(bbY2);
            var bbZ2 = tempDoc.CreateAttribute("z");
            bbZ2.Value = I_bbmaxZ;
            itemNodebbMax.Attributes.Append(bbZ2);
            itemNode.AppendChild(itemNodebbMax);

            //bsCentre
            var bsX1 = tempDoc.CreateAttribute("x");
            bsX1.Value = I_centerX;
            itemNodebsCentre.Attributes.Append(bsX1);
            var bsY1 = tempDoc.CreateAttribute("y");
            bsY1.Value = I_centerY;
            itemNodebsCentre.Attributes.Append(bsY1);
            var bsZ1 = tempDoc.CreateAttribute("z");
            bsZ1.Value = I_centerZ;
            itemNodebsCentre.Attributes.Append(bsZ1);
            itemNode.AppendChild(itemNodebsCentre);

            //bsRadius
            var val4 = tempDoc.CreateAttribute("value");
            val4.Value = I_radius;
            itemNodebsRadius.Attributes.Append(val4);
            itemNode.AppendChild(itemNodebsRadius);

            //hdTextureDist
            var val5 = tempDoc.CreateAttribute("value");
            val5.Value = hdTextureDist;
            itemNodehdTextureDist.Attributes.Append(val5);
            itemNode.AppendChild(itemNodehdTextureDist);

            //name
            itemNodeName.InnerText = name;
            itemNode.AppendChild(itemNodeName);

            //textureDictionary
            if (isUsingTextureDict == true)
            {
                itemNodetextureDictionary.InnerText = textureDictionary_TB.Text;
                itemNode.AppendChild(itemNodetextureDictionary);
            }
            else if (isUsingTextureDict == false)
            {
                itemNodetextureDictionary.InnerText = name;
                itemNode.AppendChild(itemNodetextureDictionary);
            }

            //clipDictionary
            itemNode.AppendChild(itemNodeclipDictionary);

            //textureDictionary
            if (isUsingDrawableDict == true)
            {
                itemNodeDrawableDictionary.InnerText = drawableDictionary_TB.Text;
                itemNode.AppendChild(itemNodeDrawableDictionary);
            }
            else if (isUsingDrawableDict == false)
            {
                itemNode.AppendChild(itemNodeDrawableDictionary);
            }

            //physicsDictionary
            if (isUsingPhysicsDict == true)
            {
                itemNodePhysicsDictionary.InnerText = name;
                itemNode.AppendChild(itemNodePhysicsDictionary);
            }
            else if (isUsingPhysicsDict == false)
            {
                if (embeddedCols_TB.Text.Length > 0)
                {
                    itemNodePhysicsDictionary.InnerText = embeddedCols_TB.Text;
                }
                itemNode.AppendChild(itemNodePhysicsDictionary);
            }
            //assetType
            itemNodeAssetType.InnerText = assetType;
            itemNode.AppendChild(itemNodeAssetType);

            //assetName 
            itemNodeAssetName.InnerText = name;
            itemNode.AppendChild(itemNodeAssetName);

            //extensions
            itemNode.AppendChild(itemNodeExtensions);

            if(createdYtyp == false)
            {
                CmapNodeName.InnerText = location_TB.Text;
            }
        }

        //sets all variables
        private void SetVariables()
        {

            //set textname so its not null if not using exrernal TextureDictionaryName
            textureDictionaryName = name;
            
            
            //lodDist
            lodDist = lodDist_TB.Text;

            //flags
            flags = flags_TB.Text;

            //specialAttributes
            specialAttributes = specialAttributes_TB.Text;

            //bbMin
            I_bbminX = bbMinX_TB.Text;
            I_bbminY = bbMinY_TB.Text;
            I_bbminZ = bbMinZ_TB.Text;

            //bbMax
            I_bbmaxX = bbMaxX_TB.Text;
            I_bbmaxY = bbMaxY_TB.Text;
            I_bbmaxZ = bbMaxZ_TB.Text;

            //bbCentre
            I_centerX = bsCentreX_TB.Text;
            I_centerY = bsCentreY_TB.Text;
            I_centerZ = bsCentreZ_TB.Text;

            //bbRadius
            I_radius = bsRadius_TB.Text;

            //hdTextureDist
            hdTextureDist = hdTextureDist_TB.Text;

            //name
            name = name_TB.Text;

            //assetType
            assetType = assetType_TB.Text;
        }

        //Imports all bb info from odrbrowser
        private void ImportBBs()
        {
            if (odrbrowser.bbIsCalculated == true)
            {
                //Imports BBmin's
                I_bbminName = odrbrowser.bbminName;
                I_bbminX = odrbrowser.bbminX;
                I_bbminY = odrbrowser.bbminY;
                I_bbminZ = odrbrowser.bbminZ;

                bbMinX_TB.Text = I_bbminX;
                bbMinY_TB.Text = I_bbminY;
                bbMinZ_TB.Text = I_bbminZ;

                //Imports BBmax's
                I_bbmaxName = odrbrowser.bbmaxName;
                I_bbmaxX = odrbrowser.bbmaxX;
                I_bbmaxY = odrbrowser.bbmaxY;
                I_bbmaxZ = odrbrowser.bbmaxZ;

                bbMaxX_TB.Text = I_bbmaxX;
                bbMaxY_TB.Text = I_bbmaxY;
                bbMaxZ_TB.Text = I_bbmaxZ;

                //Imports Center's
                I_centerName = odrbrowser.centerName;
                I_centerX = odrbrowser.centerX;
                I_centerY = odrbrowser.centerY;
                I_centerZ = odrbrowser.centerZ;

                bsCentreX_TB.Text = I_centerX;
                bsCentreY_TB.Text = I_centerY;
                bsCentreZ_TB.Text = I_centerZ;

                //Import Radius
                I_radiusName = odrbrowser.radiusName;
                I_radius = odrbrowser.radiusF;
                bsRadius_TB.Text = I_radius;

                double lds;
                double ir = Convert.ToDouble(I_radius);
                lds = 100 + (1.6 * ir);
                lodDist_TB.Text = lds.ToString();

                double ldd = lds * 0.65;
                hdTextureDist_TB.Text = ldd.ToString();

                //Import Model Name 
                I_modelName = odrbrowser.modelName;

                name_TB.Text = I_modelName.Replace("	", "");
            }
            else
            {
                MessageBox.Show("BB's are not calculated", "Error");
            }
        }

        //Opens odr browser
        private void OpenOdrBrowser()
        {
            odrbrowser f = new odrbrowser();
            f.Show();
        }

        //sets if using texture dictionary
        private void UsingTextureDictionary()
        {
            if (textureDictionary_TB.Visible == false)
            {
                textureDictionary_TB.Visible = true;
                isUsingTextureDict = true;

                textureDictionaryName = textureDictionary;
            }
            else if (textureDictionary_TB.Visible == true)
            {
                textureDictionary_TB.Visible = false;
                isUsingTextureDict = false;
            }
        }

        //sets if using physics dictionary
        private void UsingPhysicsDictionary()
        {
            if (isUsingPhysicsDict == false)
            {
                isUsingPhysicsDict = true;
                embeddedCols_TB.Visible = false;
            }
            else if (isUsingPhysicsDict == true)
            {
                isUsingPhysicsDict = false;
                embeddedCols_TB.Visible = true;
            }
        }

        //sets if using drawable dictionary
        private void UsingDrawableDictionary()
        {
            if (drawableDictionary_TB.Visible == false)
            {
                drawableDictionary_TB.Visible = true;
                isUsingDrawableDict = true;
            }
            else if (drawableDictionary_TB.Visible == true)
            {
                drawableDictionary_TB.Visible = false;
                isUsingDrawableDict = false;
            }
        }

        public void CreateData()
        {
            try
            {
                if(createdYtyp == true)
                {
                    tempDoc.Load(filePathSavedTo);
                }
                else
                {
                    tempDoc.Load(Application.StartupPath + "\\Resources\\templateYTYPITEM.xml");
                }

                SetVariables();

                CreateItemNode();
            }
            catch (XmlException ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void ShowViewCreatedYtypButton()
        {
            if(createdYtyp == true)
            {
                ViewCreatedYtypButton.Visible = true;
                editYTYP.filePathI = filePathSavedTo;
                editYTYP.openedFromCreator = true;
                editYTYP.fileName = filePathSavedTo;
            }
            if(createdYtyp == false)
            {
                ViewCreatedYtypButton.Visible = false;
                editYTYP.filePathI = filePathSavedTo;
                editYTYP.openedFromCreator = false;
                editYTYP.fileName = filePathSavedTo;
            }
        }

        private void ShowEnterFileName()
        {
            Enter_File_Name f = new Enter_File_Name();
            f.Show();
        }

        private void ClearTextBoxes()
        {
            name_TB.Text = "";
            lodDist_TB.Text = "";
            specialAttributes_TB.Text = "";
            flags_TB.Text = "";
            specialAttributes_TB.Text = "0";
            bsRadius_TB.Text = "";
            hdTextureDist_TB.Text = "";
            bbMinX_TB.Text = "";
            bbMinY_TB.Text = "";
            bbMinZ_TB.Text = "";
            bbMaxX_TB.Text = "";
            bbMaxY_TB.Text = "";
            bbMaxZ_TB.Text = "";
            bsCentreX_TB.Text = "";
            bsCentreY_TB.Text = "";
            bsCentreZ_TB.Text = "";
            embeddedCols_TB.Text = "";
            textureDictionary_TB.Text = "";
            drawableDictionary_TB.Text = "";
            isUsingPhysicsDictionary_CB.Checked = false;
            isUsingTextureDictionary_CB.Checked = false;
            isUsingDrawableDictionary_CB.Checked = false;
        }

        public void NewYTYP()
        {
            ClearTextBoxes();

            I_bbminName = "0";
            I_bbminX = "0";
            I_bbminY = "0";
            I_bbminZ = "0";

            I_bbmaxName = "0";
            I_bbmaxX = "0";
            I_bbmaxY = "0";
            I_bbmaxZ = "0";

            I_centerName = "0";
            I_centerX = "0";
            I_centerY = "0";
            I_centerZ = "0";

            I_radiusName = "0";
            I_radius = "0";

            I_modelName = "0";

            lodDist = "0";
            flags = "0";
            specialAttributes = "0";
            hdTextureDist = "0";
            name = "null";
            textureDictionaryName = "0";
            textureDictionary = "0";
            physicsDictionaryName = "0";
            physicsDictionary = "0";
            assetType = "0";

            isUsingTextureDict = false;
            isUsingDrawableDict = false;
            isUsingPhysicsDict = false;
            isUsingFileName = false;

            fileName = "template";
            filePathSavedTo = "";
            createdYtyp = false;
            isfn = false;

            tempDoc.Load(Application.StartupPath + "\\Resources\\templateYTYPITEM.xml");

            location_TB.Text = "";
            location_TB.Enabled = true;

            ShowViewCreatedYtypButton();
        }


        /*
         *  Creater Of This Program Is Skylumz
         *  https://www.gta5-mods.com/users/skylumz
         *  Version 1.0
        */


        //Creates YTYP.XML
        private void createButton_Click(object sender, EventArgs e)
        {
            //fileName = name;
            //SaveXML();

            if (createdYtyp == true)
            {
                SaveXML();
                location_TB.Enabled = false;
                ClearTextBoxes();
            }
            else if (createdYtyp == false)
            {
                CreateData();
                SaveXML();
                location_TB.Enabled = false;
                ClearTextBoxes();
            }
        }

        //Creates new item node WIP
        private void newItem_Click(object sender, EventArgs e)
        {
            CreateData();
        }

        //Sets texturedictionary variable
        private void isUsingTextureDictionary_CheckedChanged(object sender, EventArgs e)
        {
            UsingTextureDictionary();
        }

        //Sets drawabledictionary variable
        private void isUsingDrawableDictionary_CheckedChanged(object sender, EventArgs e)
        {
            UsingDrawableDictionary();
        }

        //Sets physicsdictionary variable
        private void isUsingPhysicsDictionary_CheckedChanged(object sender, EventArgs e)
        {
            UsingPhysicsDictionary();
        }

        //Sets isUsingCustomFileName
        private void isUsingCustomFile_CheckedChanged(object sender, EventArgs e)
        {
            if (isUsingFileName == false)
            {
                ShowEnterFileName();
                isUsingFileName = true;
            }
            else if(isUsingFileName == true)
            {
                isUsingFileName = false;
                fileName = "template";
            }
        }

        //Imports BBS
        private void importBBsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ImportBBs();
        }

        //Load editYTYP form
        private void ViewCreatedYtypButton_Click(object sender, EventArgs e)
        {
            editYTYP f = new editYTYP();
            f.Show();
        }

        private void importBBsToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            ImportBBs();
        }

        private void openToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            OpenOdrBrowser();
        }

        private void clearEverythingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ClearTextBoxes();
        }

        private void newYTYPToolStripMenuItem_Click(object sender, EventArgs e)
        {
            NewYTYP();
        }
    }
}
