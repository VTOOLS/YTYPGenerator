﻿namespace createYTYP
{
    partial class createYmap
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.modelName_TB = new System.Windows.Forms.TextBox();
            this.Parent_TB = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.Flags_TB = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.textBox13 = new System.Windows.Forms.TextBox();
            this.textBox14 = new System.Windows.Forms.TextBox();
            this.textBox15 = new System.Windows.Forms.TextBox();
            this.textBox16 = new System.Windows.Forms.TextBox();
            this.textBox17 = new System.Windows.Forms.TextBox();
            this.textBox18 = new System.Windows.Forms.TextBox();
            this.textBox19 = new System.Windows.Forms.TextBox();
            this.textBox20 = new System.Windows.Forms.TextBox();
            this.textBox21 = new System.Windows.Forms.TextBox();
            this.textBox22 = new System.Windows.Forms.TextBox();
            this.textBox23 = new System.Windows.Forms.TextBox();
            this.textBox24 = new System.Windows.Forms.TextBox();
            this.textBox25 = new System.Windows.Forms.TextBox();
            this.textBox26 = new System.Windows.Forms.TextBox();
            this.textBox27 = new System.Windows.Forms.TextBox();
            this.textBox28 = new System.Windows.Forms.TextBox();
            this.textBox29 = new System.Windows.Forms.TextBox();
            this.textBox30 = new System.Windows.Forms.TextBox();
            this.textBox31 = new System.Windows.Forms.TextBox();
            this.textBox32 = new System.Windows.Forms.TextBox();
            this.textBox33 = new System.Windows.Forms.TextBox();
            this.textBox34 = new System.Windows.Forms.TextBox();
            this.textBox35 = new System.Windows.Forms.TextBox();
            this.textBox36 = new System.Windows.Forms.TextBox();
            this.textBox37 = new System.Windows.Forms.TextBox();
            this.textBox38 = new System.Windows.Forms.TextBox();
            this.textBox39 = new System.Windows.Forms.TextBox();
            this.textBox40 = new System.Windows.Forms.TextBox();
            this.textBox41 = new System.Windows.Forms.TextBox();
            this.textBox42 = new System.Windows.Forms.TextBox();
            this.textBox43 = new System.Windows.Forms.TextBox();
            this.textBox44 = new System.Windows.Forms.TextBox();
            this.textBox45 = new System.Windows.Forms.TextBox();
            this.textBox46 = new System.Windows.Forms.TextBox();
            this.textBox47 = new System.Windows.Forms.TextBox();
            this.textBox48 = new System.Windows.Forms.TextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(843, 499);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // modelName_TB
            // 
            this.modelName_TB.Location = new System.Drawing.Point(12, 27);
            this.modelName_TB.Name = "modelName_TB";
            this.modelName_TB.Size = new System.Drawing.Size(221, 20);
            this.modelName_TB.TabIndex = 1;
            // 
            // Parent_TB
            // 
            this.Parent_TB.Location = new System.Drawing.Point(12, 53);
            this.Parent_TB.Name = "Parent_TB";
            this.Parent_TB.Size = new System.Drawing.Size(221, 20);
            this.Parent_TB.TabIndex = 2;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(12, 109);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(221, 20);
            this.textBox3.TabIndex = 4;
            // 
            // Flags_TB
            // 
            this.Flags_TB.Location = new System.Drawing.Point(12, 83);
            this.Flags_TB.Name = "Flags_TB";
            this.Flags_TB.Size = new System.Drawing.Size(221, 20);
            this.Flags_TB.TabIndex = 3;
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(12, 221);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(221, 20);
            this.textBox5.TabIndex = 8;
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(12, 195);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(221, 20);
            this.textBox6.TabIndex = 7;
            // 
            // textBox7
            // 
            this.textBox7.Location = new System.Drawing.Point(12, 165);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(221, 20);
            this.textBox7.TabIndex = 6;
            // 
            // textBox8
            // 
            this.textBox8.Location = new System.Drawing.Point(12, 139);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(221, 20);
            this.textBox8.TabIndex = 5;
            // 
            // textBox9
            // 
            this.textBox9.Location = new System.Drawing.Point(360, 221);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(221, 20);
            this.textBox9.TabIndex = 16;
            // 
            // textBox10
            // 
            this.textBox10.Location = new System.Drawing.Point(360, 195);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(221, 20);
            this.textBox10.TabIndex = 15;
            // 
            // textBox11
            // 
            this.textBox11.Location = new System.Drawing.Point(360, 165);
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(221, 20);
            this.textBox11.TabIndex = 14;
            // 
            // textBox12
            // 
            this.textBox12.Location = new System.Drawing.Point(360, 139);
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new System.Drawing.Size(221, 20);
            this.textBox12.TabIndex = 13;
            // 
            // textBox13
            // 
            this.textBox13.Location = new System.Drawing.Point(360, 109);
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new System.Drawing.Size(221, 20);
            this.textBox13.TabIndex = 12;
            // 
            // textBox14
            // 
            this.textBox14.Location = new System.Drawing.Point(360, 83);
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new System.Drawing.Size(221, 20);
            this.textBox14.TabIndex = 11;
            // 
            // textBox15
            // 
            this.textBox15.Location = new System.Drawing.Point(360, 53);
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new System.Drawing.Size(221, 20);
            this.textBox15.TabIndex = 10;
            // 
            // textBox16
            // 
            this.textBox16.Location = new System.Drawing.Point(360, 27);
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new System.Drawing.Size(221, 20);
            this.textBox16.TabIndex = 9;
            // 
            // textBox17
            // 
            this.textBox17.Location = new System.Drawing.Point(697, 221);
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new System.Drawing.Size(221, 20);
            this.textBox17.TabIndex = 24;
            // 
            // textBox18
            // 
            this.textBox18.Location = new System.Drawing.Point(697, 195);
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new System.Drawing.Size(221, 20);
            this.textBox18.TabIndex = 23;
            // 
            // textBox19
            // 
            this.textBox19.Location = new System.Drawing.Point(697, 165);
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new System.Drawing.Size(221, 20);
            this.textBox19.TabIndex = 22;
            // 
            // textBox20
            // 
            this.textBox20.Location = new System.Drawing.Point(697, 139);
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new System.Drawing.Size(221, 20);
            this.textBox20.TabIndex = 21;
            // 
            // textBox21
            // 
            this.textBox21.Location = new System.Drawing.Point(697, 109);
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new System.Drawing.Size(221, 20);
            this.textBox21.TabIndex = 20;
            // 
            // textBox22
            // 
            this.textBox22.Location = new System.Drawing.Point(697, 83);
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new System.Drawing.Size(221, 20);
            this.textBox22.TabIndex = 19;
            // 
            // textBox23
            // 
            this.textBox23.Location = new System.Drawing.Point(697, 53);
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new System.Drawing.Size(221, 20);
            this.textBox23.TabIndex = 18;
            // 
            // textBox24
            // 
            this.textBox24.Location = new System.Drawing.Point(697, 27);
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new System.Drawing.Size(221, 20);
            this.textBox24.TabIndex = 17;
            // 
            // textBox25
            // 
            this.textBox25.Location = new System.Drawing.Point(12, 470);
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new System.Drawing.Size(221, 20);
            this.textBox25.TabIndex = 32;
            // 
            // textBox26
            // 
            this.textBox26.Location = new System.Drawing.Point(12, 444);
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new System.Drawing.Size(221, 20);
            this.textBox26.TabIndex = 31;
            // 
            // textBox27
            // 
            this.textBox27.Location = new System.Drawing.Point(12, 414);
            this.textBox27.Name = "textBox27";
            this.textBox27.Size = new System.Drawing.Size(221, 20);
            this.textBox27.TabIndex = 30;
            // 
            // textBox28
            // 
            this.textBox28.Location = new System.Drawing.Point(12, 388);
            this.textBox28.Name = "textBox28";
            this.textBox28.Size = new System.Drawing.Size(221, 20);
            this.textBox28.TabIndex = 29;
            // 
            // textBox29
            // 
            this.textBox29.Location = new System.Drawing.Point(12, 358);
            this.textBox29.Name = "textBox29";
            this.textBox29.Size = new System.Drawing.Size(221, 20);
            this.textBox29.TabIndex = 28;
            // 
            // textBox30
            // 
            this.textBox30.Location = new System.Drawing.Point(12, 332);
            this.textBox30.Name = "textBox30";
            this.textBox30.Size = new System.Drawing.Size(221, 20);
            this.textBox30.TabIndex = 27;
            // 
            // textBox31
            // 
            this.textBox31.Location = new System.Drawing.Point(12, 302);
            this.textBox31.Name = "textBox31";
            this.textBox31.Size = new System.Drawing.Size(221, 20);
            this.textBox31.TabIndex = 26;
            // 
            // textBox32
            // 
            this.textBox32.Location = new System.Drawing.Point(12, 276);
            this.textBox32.Name = "textBox32";
            this.textBox32.Size = new System.Drawing.Size(221, 20);
            this.textBox32.TabIndex = 25;
            // 
            // textBox33
            // 
            this.textBox33.Location = new System.Drawing.Point(360, 470);
            this.textBox33.Name = "textBox33";
            this.textBox33.Size = new System.Drawing.Size(221, 20);
            this.textBox33.TabIndex = 40;
            // 
            // textBox34
            // 
            this.textBox34.Location = new System.Drawing.Point(360, 444);
            this.textBox34.Name = "textBox34";
            this.textBox34.Size = new System.Drawing.Size(221, 20);
            this.textBox34.TabIndex = 39;
            // 
            // textBox35
            // 
            this.textBox35.Location = new System.Drawing.Point(360, 414);
            this.textBox35.Name = "textBox35";
            this.textBox35.Size = new System.Drawing.Size(221, 20);
            this.textBox35.TabIndex = 38;
            // 
            // textBox36
            // 
            this.textBox36.Location = new System.Drawing.Point(360, 388);
            this.textBox36.Name = "textBox36";
            this.textBox36.Size = new System.Drawing.Size(221, 20);
            this.textBox36.TabIndex = 37;
            // 
            // textBox37
            // 
            this.textBox37.Location = new System.Drawing.Point(360, 358);
            this.textBox37.Name = "textBox37";
            this.textBox37.Size = new System.Drawing.Size(221, 20);
            this.textBox37.TabIndex = 36;
            // 
            // textBox38
            // 
            this.textBox38.Location = new System.Drawing.Point(360, 332);
            this.textBox38.Name = "textBox38";
            this.textBox38.Size = new System.Drawing.Size(221, 20);
            this.textBox38.TabIndex = 35;
            // 
            // textBox39
            // 
            this.textBox39.Location = new System.Drawing.Point(360, 302);
            this.textBox39.Name = "textBox39";
            this.textBox39.Size = new System.Drawing.Size(221, 20);
            this.textBox39.TabIndex = 34;
            // 
            // textBox40
            // 
            this.textBox40.Location = new System.Drawing.Point(360, 276);
            this.textBox40.Name = "textBox40";
            this.textBox40.Size = new System.Drawing.Size(221, 20);
            this.textBox40.TabIndex = 33;
            // 
            // textBox41
            // 
            this.textBox41.Location = new System.Drawing.Point(697, 470);
            this.textBox41.Name = "textBox41";
            this.textBox41.Size = new System.Drawing.Size(221, 20);
            this.textBox41.TabIndex = 48;
            // 
            // textBox42
            // 
            this.textBox42.Location = new System.Drawing.Point(697, 444);
            this.textBox42.Name = "textBox42";
            this.textBox42.Size = new System.Drawing.Size(221, 20);
            this.textBox42.TabIndex = 47;
            // 
            // textBox43
            // 
            this.textBox43.Location = new System.Drawing.Point(697, 414);
            this.textBox43.Name = "textBox43";
            this.textBox43.Size = new System.Drawing.Size(221, 20);
            this.textBox43.TabIndex = 46;
            // 
            // textBox44
            // 
            this.textBox44.Location = new System.Drawing.Point(697, 388);
            this.textBox44.Name = "textBox44";
            this.textBox44.Size = new System.Drawing.Size(221, 20);
            this.textBox44.TabIndex = 45;
            // 
            // textBox45
            // 
            this.textBox45.Location = new System.Drawing.Point(697, 358);
            this.textBox45.Name = "textBox45";
            this.textBox45.Size = new System.Drawing.Size(221, 20);
            this.textBox45.TabIndex = 44;
            // 
            // textBox46
            // 
            this.textBox46.Location = new System.Drawing.Point(697, 332);
            this.textBox46.Name = "textBox46";
            this.textBox46.Size = new System.Drawing.Size(221, 20);
            this.textBox46.TabIndex = 43;
            // 
            // textBox47
            // 
            this.textBox47.Location = new System.Drawing.Point(697, 302);
            this.textBox47.Name = "textBox47";
            this.textBox47.Size = new System.Drawing.Size(221, 20);
            this.textBox47.TabIndex = 42;
            // 
            // textBox48
            // 
            this.textBox48.Location = new System.Drawing.Point(697, 276);
            this.textBox48.Name = "textBox48";
            this.textBox48.Size = new System.Drawing.Size(221, 20);
            this.textBox48.TabIndex = 41;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(945, 25);
            this.menuStrip1.TabIndex = 49;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(39, 21);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // createYmap
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(945, 555);
            this.Controls.Add(this.textBox41);
            this.Controls.Add(this.textBox42);
            this.Controls.Add(this.textBox43);
            this.Controls.Add(this.textBox44);
            this.Controls.Add(this.textBox45);
            this.Controls.Add(this.textBox46);
            this.Controls.Add(this.textBox47);
            this.Controls.Add(this.textBox48);
            this.Controls.Add(this.textBox33);
            this.Controls.Add(this.textBox34);
            this.Controls.Add(this.textBox35);
            this.Controls.Add(this.textBox36);
            this.Controls.Add(this.textBox37);
            this.Controls.Add(this.textBox38);
            this.Controls.Add(this.textBox39);
            this.Controls.Add(this.textBox40);
            this.Controls.Add(this.textBox25);
            this.Controls.Add(this.textBox26);
            this.Controls.Add(this.textBox27);
            this.Controls.Add(this.textBox28);
            this.Controls.Add(this.textBox29);
            this.Controls.Add(this.textBox30);
            this.Controls.Add(this.textBox31);
            this.Controls.Add(this.textBox32);
            this.Controls.Add(this.textBox17);
            this.Controls.Add(this.textBox18);
            this.Controls.Add(this.textBox19);
            this.Controls.Add(this.textBox20);
            this.Controls.Add(this.textBox21);
            this.Controls.Add(this.textBox22);
            this.Controls.Add(this.textBox23);
            this.Controls.Add(this.textBox24);
            this.Controls.Add(this.textBox9);
            this.Controls.Add(this.textBox10);
            this.Controls.Add(this.textBox11);
            this.Controls.Add(this.textBox12);
            this.Controls.Add(this.textBox13);
            this.Controls.Add(this.textBox14);
            this.Controls.Add(this.textBox15);
            this.Controls.Add(this.textBox16);
            this.Controls.Add(this.textBox5);
            this.Controls.Add(this.textBox6);
            this.Controls.Add(this.textBox7);
            this.Controls.Add(this.textBox8);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.Flags_TB);
            this.Controls.Add(this.Parent_TB);
            this.Controls.Add(this.modelName_TB);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "createYmap";
            this.Text = "v";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox modelName_TB;
        private System.Windows.Forms.TextBox Parent_TB;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox Flags_TB;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.TextBox textBox13;
        private System.Windows.Forms.TextBox textBox14;
        private System.Windows.Forms.TextBox textBox15;
        private System.Windows.Forms.TextBox textBox16;
        private System.Windows.Forms.TextBox textBox17;
        private System.Windows.Forms.TextBox textBox18;
        private System.Windows.Forms.TextBox textBox19;
        private System.Windows.Forms.TextBox textBox20;
        private System.Windows.Forms.TextBox textBox21;
        private System.Windows.Forms.TextBox textBox22;
        private System.Windows.Forms.TextBox textBox23;
        private System.Windows.Forms.TextBox textBox24;
        private System.Windows.Forms.TextBox textBox25;
        private System.Windows.Forms.TextBox textBox26;
        private System.Windows.Forms.TextBox textBox27;
        private System.Windows.Forms.TextBox textBox28;
        private System.Windows.Forms.TextBox textBox29;
        private System.Windows.Forms.TextBox textBox30;
        private System.Windows.Forms.TextBox textBox31;
        private System.Windows.Forms.TextBox textBox32;
        private System.Windows.Forms.TextBox textBox33;
        private System.Windows.Forms.TextBox textBox34;
        private System.Windows.Forms.TextBox textBox35;
        private System.Windows.Forms.TextBox textBox36;
        private System.Windows.Forms.TextBox textBox37;
        private System.Windows.Forms.TextBox textBox38;
        private System.Windows.Forms.TextBox textBox39;
        private System.Windows.Forms.TextBox textBox40;
        private System.Windows.Forms.TextBox textBox41;
        private System.Windows.Forms.TextBox textBox42;
        private System.Windows.Forms.TextBox textBox43;
        private System.Windows.Forms.TextBox textBox44;
        private System.Windows.Forms.TextBox textBox45;
        private System.Windows.Forms.TextBox textBox46;
        private System.Windows.Forms.TextBox textBox47;
        private System.Windows.Forms.TextBox textBox48;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
    }
}