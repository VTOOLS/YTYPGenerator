﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO; 

namespace createYTYP
{
    public partial class createYmap : Form
    {
        public static string modelName = "template";
        public static string parentN;
        public static string flagsN;
        public static string contentFlagsN;
        public static string StreamingExtentsMinX;
        public static string StreamingExtentsMinY;
        public static string StreamingExtentsMinZ;

#pragma warning disable CS0414 // The field 'createYmap.okToSave' is assigned but its value is never used
        bool okToSave = false;
#pragma warning restore CS0414 // The field 'createYmap.okToSave' is assigned but its value is never used

        public string outPath;

        public createYmap()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            outPath = Application.StartupPath + "\\debug";
            SetVariables();
            Save(outPath);
        }

        public void Save(string outputpath)
        {

            string filename = outputpath + modelName + ".ymap.xml";


            if (File.Exists(filename))
            {
                if (MessageBox.Show("The file " + filename + " already exists. Do you want to overwrite it?", "Confirm file overwrite", MessageBoxButtons.YesNo) != DialogResult.Yes)
                {
                    return; //skip this one...
                }
            }

            StringBuilder sb = new StringBuilder();

            sb.AppendLine("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
            sb.AppendLine("<CMapData>");
            sb.AppendLine(" <name>" + modelName + "</name>");
            sb.AppendLine(" <parent>" + parentN + "</parent>");
            sb.AppendLine(" <flags value=\"" + flagsN + "\"/>");
            sb.AppendLine(" <contentFlags value=\"" + contentFlagsN + "\"/>");
            sb.AppendLine(" <streamingExtentsMin x=\"" + StreamingExtentsMinX + "\" y=\"" + StreamingExtentsMinY + "\" z=\"" + StreamingExtentsMinZ + "\"/>");
            //sb.AppendLine(" <streamingExtentsMax x=\"" + FStr(StreamingExtentsMaxX) + "\" y=\"" + FStr(StreamingExtentsMaxY) + "\" z=\"" + FStr(StreamingExtentsMaxZ) + "\"/>");
            //sb.AppendLine(" <entitiesExtentsMin x=\"" + FStr(EntitiesExtentsMinX) + "\" y=\"" + FStr(EntitiesExtentsMinY) + "\" z=\"" + FStr(EntitiesExtentsMinZ) + "\"/>");
            //sb.AppendLine(" <entitiesExtentsMax x=\"" + FStr(EntitiesExtentsMaxX) + "\" y=\"" + FStr(EntitiesExtentsMaxY) + "\" z=\"" + FStr(EntitiesExtentsMaxZ) + "\"/>");
            //sb.AppendLine(" <entities>");
            //sb.AppendLine("  <Item type=\"CEntityDef\">");
            //sb.AppendLine("   <archetypeName>" + ent.Name + "</archetypeName>");
            //sb.AppendLine("   <flags value=\"" + ent.Flags.ToString() + "\"/>");
            //sb.AppendLine("   <guid value=\"0\"/>");
            //sb.AppendLine("   <position x=\"" + FStr(ent.PosX) + "\" y=\"" + FStr(ent.PosY) + "\" z=\"" + FStr(ent.PosZ) + "\"/>");
            //sb.AppendLine("   <rotation x=\"" + FStr(ent.RotX) + "\" y=\"" + FStr(ent.RotY) + "\" z=\"" + FStr(ent.RotZ) + "\" w=\"" + FStr(ent.RotW) + "\"/>");
            //sb.AppendLine("   <scaleXY value=\"1.0\"/>");
            //sb.AppendLine("   <scaleZ value=\"1.0\"/>");
            //sb.AppendLine("   <parentIndex value=\"" + ent.NewParentIndex.ToString() + "\"/>");
            //sb.AppendLine("   <lodDist value=\"" + FStr(ent.LODDist) + "\"/>");
            //    sb.AppendLine("   <childLodDist value=\"" + FStr(ent.ChildLODDist) + "\"/>");
            //    sb.AppendLine("   <lodLevel>" + ent.LODLevel + "</lodLevel>");
            //    sb.AppendLine("   <numChildren value=\"" + ent.NumChildren.ToString() + "\"/>");
                sb.AppendLine("   <priorityLevel>PRI_REQUIRED</priorityLevel>");
                sb.AppendLine("   <extensions/>");
                sb.AppendLine("   <ambientOcclusionMultiplier value=\"255\"/>");
                sb.AppendLine("   <artificialAmbientOcclusion value=\"255\"/>");
                sb.AppendLine("   <tintValue value=\"0\"/>");
                sb.AppendLine("  </Item>");
            //sb.AppendLine(" </entities>");
            //sb.AppendLine(" <containerLods/>");
            //sb.AppendLine(" <boxOccluders/>");
            //sb.AppendLine(" <occludeModels/>");
            //sb.AppendLine(" <physicsDictionaries/>");
            //sb.AppendLine(" <instancedData>");
            //sb.AppendLine("  <ImapLink/>");
            //sb.AppendLine("  <PropInstanceList/>");
            //sb.AppendLine("  <GrassInstanceList/>");
            //sb.AppendLine(" </instancedData>");
            //sb.AppendLine(" <timeCycleModifiers/>");
            //    sb.AppendLine(" <carGenerators>");
            //        sb.AppendLine("  <Item>");
            //        sb.AppendLine("   <position x=\"" + FStr(cg.PosX) + "\" y=\"" + FStr(cg.PosY) + "\" z=\"" + FStr(cg.PosZ) + "\"/>");
            //        sb.AppendLine("   <orientX value=\"" + FStr(cg.RotX) + "\"/>");
            //        sb.AppendLine("   <orientY value=\"" + FStr(cg.RotY) + "\"/>");
            //        sb.AppendLine("   <perpendicularLength value=\"" + FStr(cg.Length) + "\"/>");
            //        //if (!string.IsNullOrEmpty(cg.ModelName))
            //        //{
            //        //    sb.AppendLine("   <carModel>" + cg.ModelName + "</carModel>"); //need to do translation!
            //        //}
            //        //else
            //        {
            //            sb.AppendLine("   <carModel/>");
            //        }
            //        sb.AppendLine("   <flags value=\"" + cg.Flags.ToString() + "\"/>");
            //        sb.AppendLine("   <bodyColorRemap1 value=\"" + cg.CarColor1.ToString() + "\"/>");
            //        sb.AppendLine("   <bodyColorRemap2 value=\"" + cg.CarColor2.ToString() + "\"/>");
            //        sb.AppendLine("   <bodyColorRemap3 value=\"" + cg.CarColor3.ToString() + "\"/>");
            //        sb.AppendLine("   <bodyColorRemap4 value=\"" + cg.SpecularColor.ToString() + "\"/>");
            //        sb.AppendLine("   <popGroup/>");
            //        sb.AppendLine("   <livery value=\"-1\"/>");
            //        sb.AppendLine("  </Item>");
            //    }
            //    sb.AppendLine(" </carGenerators>");
            //}
            //else
            //{
            //    sb.AppendLine(" <carGenerators/>");
            //}
            sb.AppendLine(" <LODLightsSOA>");
            sb.AppendLine("  <direction/>");
            sb.AppendLine("  <falloff/>");
            sb.AppendLine("  <falloffExponent/>");
            sb.AppendLine("  <timeAndStateFlags/>");
            sb.AppendLine("  <hash/>");
            sb.AppendLine("  <coneInnerAngle/>");
            sb.AppendLine("  <coneOuterAngleOrCapExt/>");
            sb.AppendLine("  <coronaIntensity/>");
            sb.AppendLine(" </LODLightsSOA>");
            sb.AppendLine(" <DistantLODLightsSOA>");
            sb.AppendLine("  <position/>");
            sb.AppendLine("  <RGBI/>");
            sb.AppendLine("  <numStreetLights value=\"0\"/>");
            sb.AppendLine("  <category value=\"0\"/>");
            sb.AppendLine(" </DistantLODLightsSOA>");
            sb.AppendLine(" <block>");
            sb.AppendLine("  <version value=\"0\"/>");
            sb.AppendLine("  <flags value=\"0\"/>");
            sb.AppendLine("  <name>" + modelName + "</name>");
            sb.AppendLine("  <exportedBy>VOPL by dexyfex</exportedBy>");
            sb.AppendLine("  <owner/>");
            sb.AppendLine("  <time>" + DateTime.UtcNow.ToString("dd MMMM yyyy HH:mm") + "</time>");
            sb.AppendLine(" </block>");
            sb.AppendLine("</CMapData>");

            File.WriteAllText(filename, sb.ToString());
        }

        public void SetVariables()
        {
            if(modelName_TB.Text.Length > 0)
            {
                modelName = modelName_TB.Text;
            }
            parentN = Parent_TB.Text;
            flagsN = Flags_TB.Text;
        }
    }
}
