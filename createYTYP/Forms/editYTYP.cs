﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using FastColoredTextBoxNS;

namespace createYTYP
{
    public partial class editYTYP : Form
    {
        OpenFileDialog openFile = new OpenFileDialog();
        SaveFileDialog saveFile = new SaveFileDialog();
        string fileLines = "";

        public static string filePathI = "";
        public static string fileName = "";

        public static bool openedFromCreator = false;

        #region Simple Functions
        public editYTYP()
        {
            InitializeComponent();
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //file_TB.Text = "";
            //openFile.FileName = "";
            this.Close();
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenYTYP();
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            toolStripStatusLabel1.Text = "New";
            file_TB.Text = "";
            //NewTab();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            file_TB.Cut();
        }

        private void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            file_TB.Copy();
        }

        private void pasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            file_TB.Paste();
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void selectAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            file_TB.SelectAll();
        }

        private void editYTYP_Load(object sender, EventArgs e)
        {
            openFile.Filter = "YTYP Files(.xml)| *.xml";
            saveFile.Filter = "YTYP Files(.xml)| *.xml";
            file_TB.Language = FastColoredTextBoxNS.Language.XML;
            file_TB.Parent = this.tabControl.TabPages["startTab"];
            file_TB.Dock = DockStyle.Fill;
            file_TB.BringToFront();
            file_TB.Text = "";


            if (openedFromCreator == true)
            {
                OpenYTYPFromCreator();
            }
        }

        private void viewHelpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Got To Add", "TODO");
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveXML();
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveXmlAs();
        }
        #endregion

        //For open YTYP from ytyp creator
        public void OpenYTYPFromCreator()
        {
            StreamReader sr1 = new StreamReader(filePathI);

            fileLines = sr1.ReadToEnd();
            file_TB.Text = fileLines;
            sr1.Close();

            toolStripStatusLabel1.Text = fileName;
        }

        //Opens YTYP File
        private void OpenYTYP()
        {
            if (openFile.ShowDialog() == DialogResult.OK)
            {
                StreamReader sr = new StreamReader(openFile.FileName);

                fileLines = sr.ReadToEnd();
                file_TB.Text = fileLines;
                sr.Close();

                toolStripStatusLabel1.Text = openFile.FileName;
            }
        }

        //For open from gtaExplorer
        public void OpenYTYPFromExplorer()
        {
            StreamReader sr1 = new StreamReader(filePathI);

            fileLines = sr1.ReadToEnd();
            file_TB.Text = fileLines;
            sr1.Close();

            toolStripStatusLabel1.Text = fileName;
        }

        //Saves XML to a new custom location
        public void SaveXmlAs()
        {
            if (saveFile.ShowDialog() == DialogResult.OK)
            {
                File.WriteAllText(saveFile.FileName, file_TB.Text);
            }
        }

        //Saves XML to pre-existing location
        public void SaveXML()
        {
           if(openedFromCreator == false)
            {
                File.WriteAllText(openFile.FileName, file_TB.Text);
            }
           else if(openedFromCreator == true)
            {
                File.WriteAllText(filePathI, file_TB.Text);
            }
        }

        public void OpenNewXML()
        {

        }

        public void NewTab()
        {
            TabPage tmpTabPage = new TabPage("New");
            tmpTabPage.Name = "New";
            tabControl.TabPages.Add(tmpTabPage);
            FastColoredTextBox tmpRichTextBox = new FastColoredTextBox();
            tabControl.SuspendLayout();
            tabControl.TabPages["New"].Controls.Add(tmpRichTextBox); 
            tabControl.ResumeLayout();
            tmpRichTextBox.Parent = this.tabControl.TabPages["New"];
            tmpRichTextBox.Dock = DockStyle.Fill;
            tmpRichTextBox.BringToFront();
        }
    }
}
