﻿namespace createYTYP
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.odrBrowserToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.yTYPCreatorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.convertYmapToXmlToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewYTYPsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(281, 25);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.odrBrowserToolStripMenuItem,
            this.yTYPCreatorToolStripMenuItem,
            this.convertYmapToXmlToolStripMenuItem,
            this.viewYTYPsToolStripMenuItem});
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(51, 21);
            this.toolsToolStripMenuItem.Text = "Tools";
            // 
            // odrBrowserToolStripMenuItem
            // 
            this.odrBrowserToolStripMenuItem.Name = "odrBrowserToolStripMenuItem";
            this.odrBrowserToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.odrBrowserToolStripMenuItem.Text = "Odr Browser";
            this.odrBrowserToolStripMenuItem.Click += new System.EventHandler(this.odrBrowserToolStripMenuItem_Click);
            // 
            // yTYPCreatorToolStripMenuItem
            // 
            this.yTYPCreatorToolStripMenuItem.Name = "yTYPCreatorToolStripMenuItem";
            this.yTYPCreatorToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.yTYPCreatorToolStripMenuItem.Text = "YTYP Creator";
            this.yTYPCreatorToolStripMenuItem.Click += new System.EventHandler(this.yTYPCreatorToolStripMenuItem_Click);
            // 
            // convertYmapToXmlToolStripMenuItem
            // 
            this.convertYmapToXmlToolStripMenuItem.Name = "convertYmapToXmlToolStripMenuItem";
            this.convertYmapToXmlToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.convertYmapToXmlToolStripMenuItem.Text = "Convert ymap to xml";
            this.convertYmapToXmlToolStripMenuItem.Visible = false;
            this.convertYmapToXmlToolStripMenuItem.Click += new System.EventHandler(this.convertYmapToXmlToolStripMenuItem_Click);
            // 
            // viewYTYPsToolStripMenuItem
            // 
            this.viewYTYPsToolStripMenuItem.Name = "viewYTYPsToolStripMenuItem";
            this.viewYTYPsToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.viewYTYPsToolStripMenuItem.Text = "Edit YTYP\'s";
            this.viewYTYPsToolStripMenuItem.Click += new System.EventHandler(this.viewYTYPsToolStripMenuItem_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(15, 28);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(242, 70);
            this.button1.TabIndex = 1;
            this.button1.Text = "Ytyp Creator";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(15, 104);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(242, 70);
            this.button2.TabIndex = 2;
            this.button2.Text = "Ytyp Editor";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(227, 92);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(54, 29);
            this.button4.TabIndex = 4;
            this.button4.Text = "Convert Ymap to Xml";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Visible = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 180);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(281, 22);
            this.statusStrip1.TabIndex = 5;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(217, 17);
            this.toolStripStatusLabel1.Text = "YTYP Generator - Author @Skylumz ";
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(281, 202);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximumSize = new System.Drawing.Size(297, 241);
            this.MinimumSize = new System.Drawing.Size(297, 241);
            this.Name = "Main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Map Editing Tools ";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem odrBrowserToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem yTYPCreatorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem convertYmapToXmlToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewYTYPsToolStripMenuItem;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
    }
}