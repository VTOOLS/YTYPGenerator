﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace createYTYP
{
    public partial class gtaExplorer : Form
    {
        public gtaExplorer()
        {
            InitializeComponent();
        }

        private void gtaExplorer_Load(object sender, EventArgs e)
        {
            string[] ids = Directory.GetLogicalDrives();
            foreach (string id in ids)
            {
                treeView1.Nodes.Add(id);
                listView1.Items.Add(id, 0);
            }
        }

        private void AssignIcons(string[] files)
        {
            foreach (string fl in files)
            {
                if (fl.EndsWith(".txt") || fl.EndsWith(".TXT"))
                {
                    listView1.Items.Add(fl, 3);
                }
                else if (fl.EndsWith(".exe") || fl.EndsWith(".EXE"))
                {
                    listView1.Items.Add(fl, 2);
                }
                else if (fl.EndsWith(".ini") || fl.EndsWith(".INI"))
                {
                    listView1.Items.Add(fl, 2);
                }
                else if (fl.EndsWith(".rpf") || fl.EndsWith(".RPF"))
                {
                    listView1.Items.Add(fl, 4);
                }
                else
                    listView1.Items.Add(fl, 3);
            }
        }

        private void ShowDriveDerects()
        {
            try
            {
                listView1.Items.Clear();

                string[] dirs = Directory.GetDirectories(treeView1.SelectedNode.Text);
                string[] files = Directory.GetFiles(treeView1.SelectedNode.Text);
                foreach(string dir in dirs)
                {
                    listView1.Items.Add(dir, 0);
                    treeView1.SelectedNode.Nodes.Add(dir);
                }

                AssignIcons(files);
                address_TB.Text = treeView1.SelectedNode.Text;
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void GoToDirectory()
        {
            try
            {
                listView1.Items.Clear();

                string[] dirs = Directory.GetDirectories(treeView1.SelectedNode.Text);
                string[] files = Directory.GetFiles(treeView1.SelectedNode.Text);
                foreach (string dir in dirs)
                {
                    listView1.Items.Add(dir, 1);
                }

                AssignIcons(files);
                if (address_TB.Items.Contains(address_TB.Text) == false)
                {
                    address_TB.Items.Add(address_TB.Text);
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void listView1_DoubleClick(object sender, EventArgs e)
        {
            string derec = listView1.SelectedItems[0].Text;

            try
            {
                listView1.Items.Clear();
                string[] dirs = Directory.GetDirectories(derec);
                string[] files = Directory.GetFiles(derec);

                foreach (string dir in dirs)
                {
                    listView1.Items.Add(dir, 1);
                }
                AssignIcons(files);
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch(Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                try
                {
                    //address_TB.Text = derec;
                    if (derec.EndsWith(".txt") || derec.EndsWith(".TXT") || derec.EndsWith(".xml") || derec.EndsWith(".XML") || derec.EndsWith(".ytyp") || derec.EndsWith(".YTYP") || derec.EndsWith(".ymap") || derec.EndsWith(".YMAP"))
                    {
                        editYTYP f = new editYTYP();
                        editYTYP.filePathI = derec;
                        f.Show();
                        f.OpenYTYPFromCreator();
                    }
                    else
                        MessageBox.Show("Cant Open This file type.", "Error");
                }
                catch(Exception ex1)
                {
                    MessageBox.Show(ex1.Message);
                }
            }
        }

        private void largeIconsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            listView1.View = View.LargeIcon;
        }

        private void smallIconsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            listView1.View = View.SmallIcon;
        }

        private void listToolStripMenuItem_Click(object sender, EventArgs e)
        {
            listView1.View = View.List;
        }
    }
}
